"""
project4.py 

UVA CS 1120 -- Problem Set 4 -- Constructing Colossi

Your name: [ Put your name here ]
Your ID:   [ Put your UVA ID here ]
"""

from lorenz import char_to_baudot, baudot_to_char, K_wheels, S_wheels, M_wheel, CIPHERTEXT

# Problem 1 

"""
You can put your answers in text here, or include a separate sheet.
"""

# Problem 2

def xor(bit1, bit2):
    """
    Returns the excusive-or of the two inputs.  You may assume the
    input values are 0 or 1, and the output should be 0 or 1.
    """
    raise RuntimeError("xor not yet implemented")

def test_xor():
    assert xor(0, 0) == 0
    assert xor(0, 1) == 1
    assert xor(1, 0) == 1
    assert xor(1, 1) == 0
    print("Passed xor tests!")

# Problem 3

def string_to_baudot(s):
    """
    Returns a list of Baudot codes corresponding to the input string of characters.
    """
    raise RuntimeError("string_to_baudot not yet implemented")

def test_string_to_baudot():
    assert string_to_baudot("CAT") == [[0, 1, 1, 1, 0], [0, 0, 0, 1, 1], [1, 0, 0, 0, 0]]

# Problem 4

def baudot_to_string(codes):
    """
    Returns a string of characters corresponding to the input Baudot codes.
    """
    raise RuntimeError("baudot_to_string not yet implemented")

def test_baudot_to_string():
    assert baudot_to_string(string_to_baudot("SECRET")) == "SECRET"

# Problem 5

"""
Describe the running time of your `baudot_to_string` procedure.  You
may do this using precise English, or using asymptotic notation. Make
sure to explain carefully what every variable you use means. 
"""

# Problem 6

def rotate_wheel(wheel):
    """
    Returns a new list that represents wheel rotated by one position.
    """
    raise RuntimeError("not yet implemented")    

def test_rotate_wheel():
    assert rotate_wheel([1,0,0,1,0]) == [0,0,1,0,1]
    assert rotate_wheel(rotate_wheel([1,0,0,1,0])) == [0,1,0,1,0]
    assert rotate_wheel([1,0,0,1,0,1,0,1,0,1]) == [0,0,1,0,1,0,1,0,1,1]
    print("Ca-ching!")

# Problem 7

def rotate_wheel_by(wheel, times):
    """
    Returns a new wheel that represents the result of rotating the
    wheel the input number of times.
    """
    raise RuntimeError("not yet implemented")    

def test_rotate_wheel_by():
    assert rotate_wheel_by([1,0,0,1,0],2) == [0,1,0,1,0]
    assert rotate_wheel_by([1,0,0,1,0],5) == [1,0,0,1,0]
    print("Badabing!")

# Problem 8

def rotate_wheel_list(wheel_list):
    """
    Returns a new list of wheels where each of the wheels in the parameter list 
    of wheels has been rotated once.
    """
    raise RuntimeError("not yet implemented")    

def test_rotate_wheel_list():
    assert rotate_wheel_list(K_wheels) == [[1,0,1,0,1], [1,0,0,1,0], [0,0,1,0,1],
                                           [1,1,0,1,1], [0,0,0,1,1]]

# Problem 9

def rotate_wheel_list_by(wheel_list, times):
    """
    Returns a new list where each of the wheels in the parameter list
    of wheels has been rotated the number parameter times.
    """
    raise RuntimeError("not yet implemented")    

def test_rotate_wheel_list_by():
    assert rotate_wheel_list_by(S_wheels, 0) == S_wheels
    assert rotate_wheel_list_by(S_wheels, 3) == [[1,1,0,0,0], [0,0,0,1,1], [0,1,0,0,1],
                                                 [0,0,1,1,0], [1,0,1,0,1]]
    assert rotate_wheel_list_by(K_wheels, 5) == K_wheels
    assert rotate_wheel_list_by(K_wheels, 25) == K_wheels
    print("Wahoowa!")

# Problem 10

"""
Describe the running time of your `rotate_wheel_list_by` procedure
(preferably using asymptotic notation, but any clear description is
acceptable).  Be sure to explain what all variables you use mean.
"""

# Problem 11

def wheel_encrypt(letter, wheels):
    """
    Returns a list that is the result of xor-ing each bit of the
    Baudot list with the first value of its respective wheel.
    """
    raise RuntimeError("not yet implemented")

def test_wheel_encrypt():
    assert wheel_encrypt([0,0,0,1,1], K_wheels) == [1,0,1,0,0]
    assert wheel_encrypt([0,0,0,1,0], K_wheels) == [1,0,1,0,1]
    assert wheel_encrypt([1,0,0,1,0], K_wheels) == [0,0,1,0,1]

# Problem 12

def do_lorenz(msg, kwheels, swheels, mwheel):
    """
    Returns the result of encrypting the msg starting from the configuration
    given by the input kwheels, swheels, and mwheel.
    """
    raise RuntimeError("not yet implemented")

def test_lorenz():
    assert do_lorenz(string_to_baudot("COOKIE"), K_wheels, S_wheels, M_wheel) == \
            [[1,1,0,1,0],[0,0,0,0,1],[1,1,0,0,1],[1,0,0,0,1],[0,0,1,1,1],[1,1,0,1,1]]
    print("Mr. Jefferson would be proud!")

# Problem 13

def lorenz_encrypt(msg, K_position, S_position, M_position):
    """
    Returns the ciphertext corresponding to the encryption of msg starting
    from the configuration given by the positions of the K, S, and M wheels.
    """
    raise RuntimeError("not yet implemented")

def test_lorenz_encrypt():
    assert lorenz_encrypt("CAKE",1,2,3) == "BNR!"
    assert lorenz_encrypt("BNR!",1,2,3) == "CAKE"
    assert lorenz_encrypt(lorenz_encrypt("JEFFERSON", 3, 1, 3), 3, 1, 3) == "JEFFERSON"
    print("I'm seeing stars now...")

# Problem 14

def brute_force_lorenz(msg):
    """
    Prints out the result of decrypting msg for all possible starting configurations. 
    """
    raise RuntimeError("not yet implemented")

# It is not necessary (please don't!) to print out all 125 decryptions
# your code should produce, but you should include the message you found.
